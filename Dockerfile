FROM python:3.7-slim
RUN mkdir /app
WORKDIR /app

ADD requirements.txt .
RUN pip install -r requirements.txt

ADD . .

EXPOSE 80
CMD ["python", "main.py", "models/prod/titanic.pkl"]
