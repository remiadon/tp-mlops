import pytest
import numpy as np


def inc_one_parent_and_children(df):
    df.loc[:, "Parch"] += 1
    return df


@pytest.mark.parametrize(
    "disturb_fn",
    [
        inc_one_parent_and_children,
        # FILLME WITH OTHER FUNCTIONS
    ],
)
def test_perturbation(model, dataset, disturb_fn):
    X, y = dataset
    X_disturbed = disturb_fn(X)
    np.testing.assert_array_equal(model.predict(X), model.predict(X_disturbed))
