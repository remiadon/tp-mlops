# TP MLOps

Ce TP a pour but de vous faire intégrer des tests orientés pour le machine learning,
ainsi qu'une méthodologie CI/CD pour déclencher ces tests, et ainsi converger vers
des modèles qui respectent des critères très précis.


## Installation et prise en main

### Conda
Tout d'abord, comme pour chaque projet Python, vous devez instancier un environnement isolé. Pour ce projet cet environnement est décrit dans `conda.yaml`
1. Installez l'environnement de développement via la commande `conda env create -f conda.yml `
2. Activez l'environnement : `conda activate tp-mlops`

### Jeux de données
Le jeu de données utiliser est le jeu de données *Titanic*, vous avez une déscription des variables présentes [ici](https://github.com/awesomedata/awesome-public-datasets/issues/351#issuecomment-358852573) 
Bien que très simple à appréhender, ce jeu de données va nous permettre de tester beaucoup de choses.
Les données sont dans le dossier `data/`, qui contient:
* un jeu d'entrainement, sur lequel appeler la méthode `fit`
* un jeu de test, sur lequel appeler `predict` qui nous sert à tester notre modèle

`Note` : ces jeux de données ont simplement été obtenu via des appels à `sklearn.model_selection.train_test_split`

### Premier modèle
Notre premier modèle sera très, très imparfait.
Pour l'obtenir, dans un terminal (i)python ou un notebook:
1. charger le jeu de données `data/train.csv`
2. Instanciez un modèle de [régression logistique](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html) avec les paramètres par défaut
3. Sauvez ce modèle via `joblib.load` dans `models/`; vous le rechargerez en mémoire via `joblib.dump` en pointant vers ce même chemin


## Gitlab CI et premiers tests

### Configuration d'un runner
Vous pouvez configurer votre propore runner, en suivant [les instructions ici](https://docs.gitlab.com/ee/ci/runners/)

Vous pouvez instancier un `Shared Runner` pour le groupe MLOPS sur gitlab, ce runner sera partagé par vous tous  


## Tests orientés pour le *Machine Learning*
### Tests d'invariance
Les tests d'invariance permette de décrire un ensemble de perturbations à effectuer sur les données en entrée sans que la réponse du modèle ne varie
Pour ces tests nous devons:
1. Instancier un fichier `src/test_invariance.py` dans lequel nous allons déclarer nos tests
2. Dans ce fichier vous devez:
2.1 Charger les données de tests depuis `data/test.csv`
2.2 Déclarer une liste de perturbations à appliquer (au moins 3)
2.3 Écrire un test pour chaque perturbation, qui
2.3.1 applique la perturbation
2.3.2 verifie que les étiquettes prédites sur les données perturbées et sur les données intactes sont les mêmes

### Slicing
Le `slicing` consiste à tester les performances du modèle sur un profile type
Vous pouvez implémenter des tests de la manière suivante
1. Créer un fichier de test dédié
2. Ecrire un liste de filtre (vous pouvez vous aider de pandas et des ses requêtes SQL)
3. Pour chaque requêtes, définissez un seuil de performance minimum
4. Mettre à  jour le fichier `.gitlab-ci.yml`, vous voulez certainement déclencher ces tests

### Tests pré-entrainement (#OPTIONEL)
Vous pouvez faire des tests de pré-entrainement. Dans une architecture de code DDD (Domain Driven Development),
ces tests feront appel:
 - au module `infrastructure` dans le cas des tests sur les données
 - au module `domain` si vous voulez tester le comportement du modèle pendant l'entrainement

 Pour commencer vous pouvez:
 1. Separer votre code en module, ou tout au moins créer un module `infrastructure`
 2. Dans le sous-dossier test de ce module, créer un sous-module `tests`
 3. utiliser un bibliothèque comme [great-expectations](https://github.com/great-expectations/great_expectations) pour veéérrifier des proprités sur le données, ou simplement pandas. Pour le jeu titanic vous pouvez vérifier que l'age est compris dans un certain rang par exemple

## Ré-entrainement du modèle
Le jeu de données titanic, avec un changement sur une colonne, peut être téléchargé via la commande 
```bash
curl -LJO https://github.com/remiadon/drifting_titanic/tree/main/drifted/train.csv
```
(Idem pour le jeu test)

Vous pouvez lancer un ré-entrainement de modèle de manière automatique, en puisant le dernier jeu via la commande au dessus


Pour ce faire, en suivant [cette page](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
1. Instanciez in **Gitlab Pipeline Schedule** avec un systaxe cron. Cette objet décris à  quel pas de temps nous allons déclencher un action. Le site [Cron Guru](https://crontab.guru/) vous donne une bonne idée des syntaxes possibles. Nous allons **ré-entrainer notre modèle toutes les heures**
2. Mofier votre `.gitlab-ci.yml` pour lancer votre script de ré-entrainement.
3. Utiliser les `artifacts` de gitlab pour garder un réssultats, et la syntaxe `only: changes:` pour détecter un dépot dans le dossier sur lequel l'artefact pointe



## Métriques

### Metrics via gitlab CI
Gitlab CI permet la création de [métriques](https://docs.gitlab.com/ee////////ci/metrics_reports.html), accessibles lors de vos "Merge Request"

1. Reéécriver vos test de performance dans `src/test_post_train.py` pour sortir des meétriques au format .txt
2. Modifier le fichier `.gitlab-ci.yml` afin d'utiliser ces métriques et pouvoir les comparer à celles obtenus sur votre branche master
3. Ces métriques devront être lancées lors du merge de votre branche vers `preprod`

### Air Speed Velocity (#OPTIONEL)
En local, vous pouvez instancier `asv` via la commande `asv quickstart`
Suivez les instructions sur [cette page](https://asv.readthedocs.io/en/stable/using.html) pour le mettre en place
N'ouvliez pas de lancer `asv run` à  la fin

Ensuite
1. Créez un branche séparée qui contiendra un nouveau modèle
2. modifiez le fichier `src/fit_new_model.py` pour y entrainer un autre modèle, qui se revelera plus ou moins perfomante
3. reééeéévri vos tests preéésents dans `src/test_post_train.py` en suivant les syntaxes décrites [ici](https://asv.readthedocs.io/en/stable/writing_benchmarks.html)
3. Lancez `asv compare master nouvelle-branche` et comparez les deux branche


*Note* Attention, la mise en place d'ASV dans la CI se révéle très, très compliquee
### Bonus
déclenchez `asv compare` seulement lorsqu'un changement est detecté dans le dossier `models/`


### Gestion de l'env via conda
Pour utiliser conda dans la gestion de vos pipelines gitlab CI, [voici un tuto](https://beenje.github.io/blog/posts/gitlab-ci-and-conda/)
