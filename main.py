from flask import Flask, jsonify, request
import joblib
import pandas as pd
import sys


app = Flask(__name__)


@app.route("/predict", methods=["POST"])
def predict():
    request_data = request.get_json()

    path = request_data["path"]
    df = pd.read_csv(path, header=0)
    preds = clf.predict(df)
    return jsonify({"prediction": preds.tolist()})


if __name__ == "__main__":
    if len(sys.argv) > 0:
        path = "models/prod/titanic.pkl"
    else:
        path = sys.argv[1]
    clf = joblib.load(path)
    app.run(debug=True, port=8080)
